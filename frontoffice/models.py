from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from utilities.slugify import unique_slugify
from utilities.validators import is_true


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    address = models.CharField(max_length=500, null=True, blank=True)
    image_url = models.URLField(null=True, blank=True)
    is_email_verified = models.BooleanField(default=False, )
    has_accepted_terms_and_conditions = models.BooleanField(validators=[is_true], default=False, )

    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    @property
    def username(self):
        return self.user.username

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    def __unicode__(self):
        return u'%s' % self.user.username


class Event(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    location = models.CharField(max_length=500, null=True, blank=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    last_update_time = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        ordering = ('-creation_time', 'slug', )

    def save(self, *args, **kwargs):
        unique_slugify(self, self.title)
        super(Event, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.title


class Tag(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
        ordering = ('slug', )

    def save(self, *args, **kwargs):
        unique_slugify(self, self.name)
        super(Tag, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.name


class Deal(models.Model):
    SEARCH = 0
    OFFER = 1

    DEAL_CATEGORY = (
        (SEARCH, _('Search')),
        (OFFER, _('Offer')),
    )

    owner = models.ForeignKey(UserProfile)
    event = models.ForeignKey(Event)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    category = models.IntegerField(choices=DEAL_CATEGORY, default=SEARCH)
    creation_time = models.DateTimeField(auto_now_add=True)
    last_update_time = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    tags = models.ManyToManyField(Tag)

    @property
    def category_name(self):
        return self.DEAL_CATEGORY[self.category][1]

    class Meta:
        verbose_name = _('Deal')
        verbose_name_plural = _('Deals')
        ordering = ('-creation_time', 'slug', )

    def save(self, *args, **kwargs):
        unique_slugify(self, self.title)
        super(Deal, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.title
