from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from .forms import SearchForm, DealForm
from .models import UserProfile, Event, Deal
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext


# ===========================================================================
# HOME
# ===========================================================================
def home(request, template_name="frontoffice/home.html"):
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'search_form': SearchForm(),
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def display_profile(request, template_name="frontoffice/profile.html"):
    user = get_object_or_404(UserProfile, user=request.user)
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'user': user,
        'deals': Deal.objects.filter(owner=user),
    }, context_instance=RequestContext(request))


# ===========================================================================
# SEARCH
# ===========================================================================
def display_search_results(request, template_name="frontoffice/results.html"):
    search_form = SearchForm()

    # Get queried data ('query' and 'page')
    if request.method == 'POST' and 'query' in request.POST:
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            query = request.POST['query']
        else:
            query = ""
    elif request.GET.get('query'):
        query = request.GET.get('query')
    else:
        return HttpResponseRedirect(reverse('frontoffice_home'))

    page = request.GET.get('page')

    # Search for Price objects in DB
    results = Deal.objects.filter(Q(title__icontains=query)
                                  | Q(description__icontains=query)
                                  | Q(event__location__icontains=query)
                                  | Q(tags__name__icontains=query)).distinct()

    counter = results.count()

    # Implement pagination
    paginator = Paginator(results, settings.RESULTS_PER_PAGE)
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        results = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        results = paginator.page(paginator.num_pages)

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'search_form': search_form,
        'results': results,
        'query': query,
        'page': page,
        'counter': counter,
    }, context_instance=RequestContext(request))



# ===========================================================================
# EVENTS
# ===========================================================================
def display_events(request, template_name="frontoffice/home.html"):
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'events': Event.objects.filter(is_active=True),
    }, context_instance=RequestContext(request))


def display_event(request, event_slug, template_name="frontoffice/home.html"):
    event = get_object_or_404(Event, slug=event_slug)
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'event': event,
    }, context_instance=RequestContext(request))


def display_deals_by_event(request, event_slug, template_name="frontoffice/deals_by_event.html"):
    event = get_object_or_404(Event, slug=event_slug)
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'event': event,
        'deals': Deal.objects.filter(event=event),
    }, context_instance=RequestContext(request))


def display_deal(request, deal_slug, template_name="frontoffice/deal.html"):
    deal = get_object_or_404(Deal, slug=deal_slug)
    return render_to_response(template_name, {
        'session': request.session.keys(),
        'deal': deal,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def deal_create(request, template_name="frontoffice/deal_create.html"):
    user = get_object_or_404(UserProfile, user=request.user)
    if request.method == 'POST':
        deal_form = DealForm(request.POST)
        if deal_form.is_valid():
            deal = deal_form.save(commit=False)
            deal.owner = user
            deal.save()
            return HttpResponseRedirect(reverse('frontoffice_profile'))
    else:
        deal_form = DealForm()

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'deal_form': deal_form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def deal_edit(request, deal_slug, template_name="frontoffice/deal_edit.html"):
    deal = get_object_or_404(Deal, slug=deal_slug)

    if request.method == 'POST':
        deal_form = DealForm(request.POST, instance=deal)
        if deal_form.is_valid():
            deal_form.save()
            return HttpResponseRedirect(reverse('frontoffice_profile'))
    else:
        deal_form = DealForm(instance=deal)

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'deal': deal,
        'deal_form': deal_form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def deal_delete(request, deal_slug):
    deal = get_object_or_404(Deal, slug=deal_slug)
    deal.delete()
    return HttpResponseRedirect(reverse('frontoffice_profile'))
