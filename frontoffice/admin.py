from django.contrib import admin
from .models import UserProfile, Tag, Deal, Event



class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('username',
                    'first_name',
                    'last_name',
                    )
    list_display_links = ('username', )
    save_on_top = True
    list_per_page = 20


class EventAdmin(admin.ModelAdmin):
    list_display = ('title',
                    'creation_time',
                    'last_update_time',
                    'is_active',
                    )
    save_on_top = True


class TagAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'is_active',
                    )
    save_on_top = True


class DealAdmin(admin.ModelAdmin):
    list_filter = ('tags',
                   'category',
                   )
    list_display = ('title',
                    'owner',
                    'event',
                    'creation_time',
                    'last_update_time',
                    'is_active',
                    )
    save_on_top = True


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Deal, DealAdmin)
