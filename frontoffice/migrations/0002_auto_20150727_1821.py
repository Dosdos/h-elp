# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontoffice', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('slug', models.SlugField(unique=True, max_length=255, blank=True)),
                ('description', models.CharField(max_length=500, null=True, blank=True)),
                ('category', models.IntegerField(default=0, choices=[(0, 'Search'), (1, 'Offer')])),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_update_time', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('-creation_time', 'slug'),
                'verbose_name': 'Deal',
                'verbose_name_plural': 'Deals',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('slug', models.SlugField(unique=True, max_length=255, blank=True)),
                ('description', models.CharField(max_length=500, null=True, blank=True)),
                ('location', models.CharField(max_length=500, null=True, blank=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_update_time', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('-creation_time', 'slug'),
                'verbose_name': 'Event',
                'verbose_name_plural': 'Events',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.SlugField(unique=True, max_length=255, blank=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('slug',),
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.AddField(
            model_name='userprofile',
            name='address',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='image_url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='deal',
            name='event',
            field=models.ForeignKey(to='frontoffice.Event'),
        ),
        migrations.AddField(
            model_name='deal',
            name='owner',
            field=models.ForeignKey(to='frontoffice.UserProfile'),
        ),
        migrations.AddField(
            model_name='deal',
            name='tags',
            field=models.ManyToManyField(to='frontoffice.Tag'),
        ),
    ]
