from django.conf.urls import patterns


urlpatterns = patterns(
    'frontoffice.views',

    # Home
    (r'^[/]?$', 'home', {'template_name': 'frontoffice/home.html'}, 'frontoffice_home'),

    # Profile
    (r'^profile[/]?$', 'display_profile', {'template_name': 'frontoffice/profile.html'}, 'frontoffice_profile'),

    # Search
    (r'^search[/]?$', 'display_search_results', {'template_name': 'frontoffice/results.html'}, 'search'),

    # Events
    (r'^events[/]?$', 'display_events', {'template_name': 'frontoffice/events.html'}, 'frontoffice_events'),
    (r'^event/(?P<event_slug>[-\w]+)[/]?$', 'display_event', {'template_name': 'frontoffice/event.html'}, 'frontoffice_event'),
    (r'^event/(?P<event_slug>[-\w]+)/deals[/]?$', 'display_deals_by_event', {'template_name': 'frontoffice/deals_by_event.html'}, 'frontoffice_deals_by_event'),

    # Deal
    (r'^deal/(?P<deal_slug>[-\w]+)[/]?$', 'display_deal', {'template_name': 'frontoffice/deal.html'}, 'frontoffice_deal'),
    (r'^add_deal[/]?$', 'deal_create', {'template_name': 'frontoffice/deal_create.html'}, 'frontoffice_deal_create'),
    (r'^deal/(?P<deal_slug>[-\w]+)/edit[/]?$', 'deal_edit', {'template_name': 'frontoffice/deal_edit.html'}, 'frontoffice_deal_edit'),
    (r'^deal/(?P<deal_slug>[-\w]+)/delete[/]?$', 'deal_delete', {}, 'frontoffice_deal_delete'),

)
